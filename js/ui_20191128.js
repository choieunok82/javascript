// 16_1 반복문

for (let i=0; i<5; i++){ // cost라고 안 적은 이유는 i가 계속 변하기 때문에 left라고 함.
    console.log('안녕하세요',i)
}

for (let i=0, j=5; i<j; i++){ 
    console.log('안녕하세요',i, j)
}

for (let i=0, j=2; i<5; i++, j=j*j){
    console.log('안녕하세요', i, j)
}

//반목문을 즉시 종료하고 싶을 때는 반복되는 블럭 안에서 break;를 실행하면 가능합니다.

for (let i=0; i<5; i++){ 
    console.log(i);
    if(i>2){
        break;
    }
    console.log('안녕하세요 break', i);
}

//반복되는 블럭 안에서 continue;를 만나면 거기서 바로 해당 블럭을 종료합니다.
//그리고 이와 같이 다음 반복이 있으면 다음 반복으로 넘어갑니다.
for (let i=0; i<5; i++){ 
    console.log(i);
    if(i<2){
        continue;
    }
    console.log('안녕하세요', i);
}

//for 무한 루프
// for (;;) {
//     console.log('안녕하세요');
//     if(Math.random() * 100 > 90){
//         break;
//     }
// }